﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Kollektsioonidest
{
    class Inimene
    {
        public string Nimi;
        public int Vanus;

        public override string ToString()
        {
            return $"inimene {Nimi} vanusega {Vanus}";
        }

        public static Inimene Parse(string i)
        {
            var ii = i.Split(',').Select(x => x.Trim()).ToArray();
            return new Inimene { Nimi = ii[0], Vanus = ii.Length> 1 ? int.Parse(ii[1]) : 0 };
        }
    }


    static class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            TrykiArvud(arvud.Where(x => x % 2 == 0));
            //public static bool KasPaaris(this int x) => x % 2 == 0;

            List<int> teisedarvud = new List<int> { 2, 7, 3, 8, 9, 1 };

            teisedarvud   // enumeraabel
                .Where(x => x % 2 != 0) // enumeraabel
                .TrykiArvud();
            //public static bool KasPaaritu(this int x) => x % 2 != 0;

            int p = 3;
            teisedarvud.Where(x => x % p == 0).TrykiArvud();


            var õ = teisedarvud.Where(x => x % 2 == 0);

            var Inimesed =
            File.ReadAllLines(@"..\..\test.txt")
                .Skip(1)
                .Select(x => Inimene.Parse(x)).ToList();

            // see siin on hulga avaldis extensionitega
            var q1 = Inimesed
                .Where(x => x.Vanus > 20)
                .OrderBy( x => x.Nimi)
                .Select(x => x)
                ;

            // see on TÄPSELT sama, aga LINQ avaldisena
            var q2 = from x in Inimesed
                     where x.Vanus > 20
                     orderby x.Nimi
                     select x;

            foreach (var x in q2) Console.WriteLine(x);


        }

        public static void HForEach<T>(this IEnumerable<T> m, Action<T> a)
        {
            foreach (var x in m) a(x);
        }


        //public static void Tryki(this IEnumerable<string> t)
        //   { foreach (var x in t) Console.WriteLine(x);}
	

        public static  void TrykiArvud(this IEnumerable<int> m)
        {
            int summa = 0;
            foreach (var x in m) 
            {
                summa += x;
                Console.WriteLine(x);
            }
            Console.WriteLine("Summa on {0}", summa);
        }

        public static IEnumerable<T> Kas<T>(this IEnumerable<T> m, Func<T, bool> f)
        {
            foreach(var x in m) if (f(x)) yield return x;
            
        }




    }
}
